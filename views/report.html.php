<table>
    <thead>
        <tr>
            <th>Branch</th>
            <th>HARPOON</th>
            <th>Chars</th>
        </tr>
    <thead>
    <tbody>
        <?php foreach($harpoons as $harpoon): ?>
            <tr>
                <td><?= $harpoon['branch'] ?></td>
                <td><?= $harpoon['text'] ?></td>
                <td><?= $harpoon['char_count'] ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
