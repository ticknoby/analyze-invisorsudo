#Harpoons

|Branch|Harpoon|Chars|Bytes|
|:-|:-|:-|:-|
<?php foreach($harpoons as $harpoon): ?>
|<?= $harpoon['branch'] ?>
|<?= $harpoon['text'] ?>
|<?= $harpoon['char_count'] ?>
|<?= $harpoon['bytes'] ?>
|
<?php endforeach; ?>
