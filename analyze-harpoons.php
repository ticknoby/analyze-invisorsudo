<?php

use Blah\Repo;
use Blah\HarpoonAnalyzer;
use Blah\HarpoonExtractor;
use Blah\ReportWriter;
use Blah\ImageGenerator;

require_once('./bootstrap.php');

$origin = 'https://ticknoby@bitbucket.org/invisordevstuff/invisorsudo.git';

$repo = new Repo($origin, './repo/');
$repo->init();

$harpoon_storage = './output/harpoons';
if (!file_exists($harpoon_storage)) {
    (new HarpoonExtractor($repo, $harpoon_storage))
        ->extractAllHarpoons();
}

$analyzer = new HarpoonAnalyzer($harpoon_storage);
$harpoons = $analyzer->getHarpoons();

$mobydick = file_get_contents('./cfg/loomings'); 
$images = './output/images/';
if (!file_exists($images)) {
    mkdir($images);
    (new ImageGenerator($mobydick, $harpoons, $images))
        ->generateAllImages();
}

(new ReportWriter($harpoons))
    ->writeReport('./output/report.md');
