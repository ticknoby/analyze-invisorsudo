<?php

use Blah\Repo;

namespace Blah;

class HarpoonExtractor {
    protected $repo;
    protected $dest;
    
    public function __construct(Repo $repo, $dest_file) {
        $this->repo = $repo;
        $this->dest = $dest_file;
    }
    
    public function extractAllHarpoons(): void {
        $repo = $this->repo;
        $numbered_branches = array_filter($repo->getLocalBranches(), 'is_numeric');
        
        $dest = fopen($this->dest, 'x');
        try {
            foreach ($numbered_branches as $branch) {
                $repo->checkout($branch);
                $line = sprintf("%s %s\n", $branch, $this->scrapeHarpoon());
                fwrite($dest, $line);
            }            
        }
        finally {
            fclose($dest);
        }        
    }
    
    public function scrapeHarpoon(): string {
        $subject = file_get_contents($this->repo->getLocalDir() . 'harpoon.inc');
        $pattern = "/(?<=HARPOON', ')(.*?)(?='\);)/";
        preg_match($pattern, $subject, $matches);
        return $matches[0];
    }
}
