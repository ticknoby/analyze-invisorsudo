<?php

use Blah\Repo;

namespace Blah;

class ReportWriter
{
    protected $analyzer;
    
    public function __construct($harpoons) {
        $this->harpoons = $harpoons;
    }
    
    public function writeReport($filename): void {
        $file = fopen($filename, 'w');
        try {
            $this->_writeReport($file);       
        }
        finally {
            fclose($file);
        }     
    }
    
    protected function _writeReport($file): void {
        $harpoons = $this->harpoons;
        
        ob_start();
        include(ROOT . '/views/report.md.php');
        $report = ob_get_contents();
        ob_end_clean();        
        fwrite($file, $report);
    }
}
