<?php

namespace Blah;


class Repo {    
    protected $dir;
    protected $origin;
    
    public function __construct(string $origin, string $local_dir) {
        $this->origin = $origin;
        $this->dir = $local_dir;
    }
    
    public function init() {
        if (file_exists($this->dir)) {
            return;
        }
        mkdir($this->dir);
        $this->exec(sprintf('git clone %s .', $this->origin));
        foreach ($this->getRemoteBranches() as $branch) {
            $this->checkout($branch, true);
        }
    }
    
    public function checkout(string $branch, $track = false) {
        $track = $track ? '--track' : '';
        $this->exec(sprintf('git checkout %s %s', $branch, $track));
    }
    
    public function getRemoteBranches(): array {        
        return array_map('trim', $this->exec('git branch -r'));
    }
    
    public function getLocalBranches(): array {
        return array_map('trim', $this->exec('git branch'));
    }
    
    public function getLocalDir(): string {
        return $this->dir;
    }

    /**
     * not secure!
     */
    protected function exec($command) {
        exec(sprintf('cd %s && %s', $this->dir, $command), $output);
        return $output;
    }
    
}
