<?php

use Blah\Repo;

namespace Blah;

class ImageGenerator
{
    protected $mobydick;
    protected $harpoons;
    protected $dir;    
    
    public function __construct($mobydick, $harpoons, $dir) {
        $this->mobydick = $mobydick;
        $this->harpoons = $harpoons;
        $this->dir = $dir;
    }
    
    public function generateAllImages() {
        foreach ($this->harpoons as $harpoon) {
            $image = \ishmael($this->mobydick, $harpoon['text']);
            $dest = sprintf('%s%s.jpg', $this->dir, $harpoon['branch']);
            $this->base64ToJpeg($image, $dest);
        }
    }
    
    /**
     * https://stackoverflow.com/questions/15153776/convert-base64-string-to-an-image-file
     */
    protected function base64ToJpeg($base64_string, $filename) {
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode(',', $base64_string);
        
        if ($decoded = base64_decode($data[1])) {
            $file = fopen($filename, 'w'); 
            fwrite($file, $decoded);
            fclose($file); 
        } 
    }
    
}
