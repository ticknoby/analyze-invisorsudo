<?php

use Blah\Repo;

namespace Blah;

class HarpoonAnalyzer
{
    protected $filename;
    protected $harpoons;
    
    public function __construct($filename) {
        $this->filename = $filename;
    }
    
    protected function loadHarpoons() {        
        $lines = file($this->filename, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        
        $this->harpoons = array_map(function($line) {
            list($branch, $text) = explode(' ', $line, 2);
            
            return [
                'branch' => $branch,
                'text' => $text,
                'char_count' => mb_strlen($text),
                'bytes' => strlen($text)
            ];
        }, $lines);
    }
    
    public function getHarpoons() {
        if (!$this->harpoons) {
            $this->loadHarpoons();
        }
        return $this->harpoons;
    }
        
}
