#Harpoons

|Branch|Harpoon|Chars|Bytes|
|:-|:-|:-|:-|
|12730|im a coat, which he sadly needed, or invest his money in a pedestrian trip to Rockaway Beach? Why is almost every robust h|122|122|
|14049|t a still better seaward peep. But these are all landsmen; of week days pent up in lath and plaster—tied to counters, na|120|122|
|14545|r it is a damp, drizzly November in my soul; whenever I find myself involuntarily pausing before coffin warehouses, and br|122|122|
|16631|n the pier-heads; some looking over the bulwarks of ships from China; some high aloft in the rigging, as if striving to ge|122|122|
|16856|yage was welcome; the great flood-gates of the wonder-world swung open, and in the wild conceits that swayed me to my purp|122|122|
|17054|is not a drop of water there! Were Niagara but a cataract of sand, would you travel your thousand miles |104|104|
|17401|view, that is; and so the universal thump is passed round, and all hands should rub each other’s shoulder-blades, and be|120|122|
|17633|family in the land, the Van Rensselaers, or Randolphs, or Hardicanutes. And more than all, if just previous to putting you|122|122|
|19472|—commerce surrounds it with her surf. Right and left, the streets take you waterward. Its extreme downtown is the batter|120|122|
|23153| supplied with a metaphysical professor. Yes, as every one knows, meditation and water are wedded for ever. But here is an|122|122|
|23467|Look at the crowds of water-gazers there. Circumambulate the city of a dreamy Sabbath afternoon. Go from Corlears Hook to |122|122|
|23781|e most part the Commodore on the quarter-deck gets his atmosphere at second hand from the sailors on the forecastle. He th|122|122|
|23945|asp the tormenting, mild image he saw in the fountain, plunged into it and was drowned. But that same image, we ourselves |122|122|
|24334|what is good, I am quick to perceive a horror, and could still be social with it—would they let me—since it is but wel|118|122|
|26396|inging up the rear of every funeral I meet; and especially whenever my hypos get such an upper hand of me, that it require|122|122|
|26983|lways go to sea as a sailor, because of the wholesome exercise and pure air of the fore-castle deck. For as in this world,|122|122|
|27355|ted with an everlasting itch for things remote. I love to sail forbidden seas, and land on barbarous coasts. Not ignoring |122|122|
|27646|hats off—then, I account it high time to get to sea as soon as I can. This is my substitute for pistol and ball. With a |120|122|
|27669|ome from lanes and alleys, streets and avenues—north, east, south, and west. Yet here they all unite. Tell me, does the |120|122|
|27693|riminating judgment. Chief among these motives was the overwhelming idea of the great whale himself. Such a portentous and|122|122|
|27912|r answer than any one else. And, doubtless, my going on this whaling voyage, formed part of the grand programme of Provide|122|122|
|28217|wn, stand thousands upon thousands of mortal men fixed in ocean reveries. Some leaning against the spiles; some seated upo|122|122|
|28578|, I go as a simple sailor, right before the mast, plumb down into the forecastle, aloft there to the royal mast-head. True|122|122|
|28870|nd a crucifix were within; and here sleeps his meadow, and there sleep his cattle; and up from yonder cottage goes a sleep|122|122|
|30388|eds have a purse, and a purse is but a rag unless you have something in it. Besides, passengers get sea-sick—grow quarre|120|122|
|31910|ntry; in some high land of lakes. Take almost any path you please, and ten to one it carries you down in a dale, and leave|122|122|
|32748| of my lungs, I do not mean to have it inferred that I ever go to sea as a passenger. For to go as a passenger you must ne|122|122|
|33431|r hand into the tar-pot, you have been lording it as a country schoolmaster, making the tallest boys stand in awe of you. |122|122|
|33721|alley of the Saco. What is the chief element he employs? There stand his trees, each with a hollow trunk, as if a hermit a|122|122|
|34204| head winds are far more prevalent than winds from astern (that is, if you never violate the Pythagorean maxim), so for th|122|122|
|34280| of the New Testament? Do you think the archangel Gabriel thinks anything the less of me, because I promptly and respectfu|122|122|
|37447|ll that region. Should you ever be athirst in the great American desert, try this experiment, if your caravan happen to be|122|122|
|37725|land? Why did the old Persians hold the sea holy? Why did the Greeks give it a separate deity, and own brother of Jove? Su|122|122|
|38153|eries—stand that man on his legs, set his feet a-going, and he will infallibly lead you to water, if water there be in a|120|122|
|38750|Prairies in June, when for scores on scores of miles you wade knee-deep among Tiger-lilies—what is the one charm wanting|120|122|
|39063| hooded phantom, like a snow hill in the air.|45|45|
|40610| I say that I am in the habit of going to sea whenever I begin to grow hazy about the eyes, and begin to be over conscious|122|122|
|42566|see in all rivers and oceans. It is the image of the ungraspable phantom of life; and this is the key to it all. Now, when|122|122|
|42869|ously buttered, and judgmatically salted and peppered, there is no one who will speak more respectfully, not to say revere|122|122|
|44169|, and that on no account can a monied man enter heaven. Ah! how cheerfully we consign ourselves to perdition! Finally, I a|122|122|
|45937|magnetic virtue of the needles of the compasses of all those ships attract them thither? Once more. Say you are in the cou|122|122|
|46241| mysterious monster roused all my curiosity. Then the wild and distant seas where he rolled his island bulk; the undeliver|122|122|
|46677|chant sailor, I should now take it into my head to go on a whaling voyage; this the invisible police officer of the Fates,|122|122|
|46956|The transition is a keen one, I assure you, from a schoolmaster to a sailor, and requires a strong decoction of Seneca and|122|122|
|47714|ulations of every kind whatsoever. It is quite as much as I can do to take care of myself, without taking care of ships, b|122|122|
|48294|his sort of thing is unpleasant enough. It touches one’s sense of honor, particularly if you come of an old established |120|122|
|49884|s a strong moral principle to prevent me from deliberately stepping into the street, and methodically knocking people’s |120|122|
|50158|sted river horse, that you see the mummies of those creatures in their huge bake-houses the pyramids. No, when I go to sea|122|122|
|53766| pay passengers a single penny that I ever heard of. On the contrary, passengers themselves must pay. And there is all the|122|122|
|55374|cular to interest me on shore, I thought I would sail about a little and see the watery part of the world. It is a way I h|122|122|
|55749|re crowds, pacing straight for the water, and seemingly bound for a dive. Strange! Nothing will content them but the extre|122|122|
|56962|tain orders me to get a broom and sweep down the decks? What does that indignity amount to, weighed, I mean, in the scales|122|122|
|59235|s you there by a pool in the stream. There is magic in it. Let the most absent-minded of men be plunged in his deepest rev|122|122|
|60850|y, where that noble mole is washed by waves, and cooled by breezes, which a few hours previous were out of sight of land. |122|122|
|61346|lsome—don’t sleep of nights—do not enjoy themselves much, as a general thing;—no, I never go as a passenger; nor, |114|122|
|62059| the Stoics to enable you to grin and bear it. But even this wears off in time. What of it, if some old hunks of a sea-cap|122|122|
|62240|epherd’s head, yet all were vain, unless the shepherd’s eye were fixed upon the magic stream before him. Go visit the |118|122|
|62505|of the United States. “WHALING VOYAGE BY ONE ISHMAEL. “BLOODY BATTLE IN AFFGHANISTAN.” Though I cannot tell why it w|116|122|
|64299|, they rather order me about some, and make me jump from spar to spar, like a grasshopper in a May meadow. And at first, t|122|122|
|66215|lly obey that old hunks in that particular instance? Who ain’t a slave? Tell me that. Well, then, however the old sea-ca|120|122|
|68178|though I cannot tell why this was exactly; yet, now that I recall all the circumstances, I think I can see a little into t|122|122|
|68655| artist. He desires to paint you the dreamiest, shadiest, quietest, most enchanting bit of romantic landscape in all the v|122|122|
|69523|ssenger, did you yourself feel such a mystical vibration, when first told that you and your ship were now out of sight of |122|122|
|70982|s. I take it that this part of the bill must have run something like this: “Grand Contested Election for the Presidency |120|122|
|70999|to see it? Why did the poor poet of Tennessee, upon suddenly receiving two handfuls of silver, deliberate whether to buy h|122|122|
|71421|as exactly that those stage managers, the Fates, put me down for this shabby part of a whaling voyage, when others were se|122|122|
|72052|inction of such offices to those who like them. For my part, I abominate all honorable respectable toils, trials, and trib|122|122|
|78322|ealthy boy with a robust healthy soul in him, at some time or other crazy to go to sea? Why upon your first voyage as a pa|122|122|
|78670|e blue. But though the picture lies thus tranced, and though this pine-tree shakes down its sighs like leaves upon this sh|122|122|
|80159|ptains may order me about—however they may thump and punch me about, I have the satisfaction of knowing that it is all r|120|122|
|80957|y smoke. Deep into distant woodlands winds a mazy way, reaching to overlapping spurs of mountains bathed in their hill-sid|122|122|
|81945|he ocean with me. There now is your insular city of the Manhattoes, belted round by wharves as Indian isles by coral reefs|122|122|
|82931|ave of driving off the spleen and regulating the circulation. Whenever I find myself growing grim about the mouth; wheneve|122|122|
|83631|a cook being a sort of officer on ship-board—yet, somehow, I never fancied broiling fowls;—though once broiled, judici|118|122|
|84048|ose, two and two there floated into my inmost soul, endless processions of the whale, and, mid most of them all, one grand|122|122|
|84104|Call me Ishmael. Some years ago—never mind how long precisely—having little or no money in my purse, and nothing parti|118|122|
|85472|mest limit of the land; loitering under the shady lee of yonder warehouses will not suffice. No. They must get just as nig|122|122|
|86404|l to be on friendly terms with all the inmates of the place one lodges in. By reason of these things, then, the whaling vo|122|122|
|86586|nce that was drawn up a long time ago. It came in as a sort of brief interlude and solo between more extensive performance|122|122|
|86587|he part I did, besides cajoling me into the delusion that it was a choice resulting from my own unbiased freewill and disc|122|122|
|87160|philosophical flourish Cato throws himself upon his sword; I quietly take to the ship. There is nothing surprising in this|122|122|
|87393|t down for magnificent parts in high tragedies, and short and easy parts in genteel comedies, and jolly parts in farces—|120|122|
|88251|. If they but knew it, almost all men in their degree, some time or other, cherish very nearly the same feelings towards t|122|122|
|88954|able, nameless perils of the whale; these, with all the attending marvels of a thousand Patagonian sights and sounds, help|122|122|
|89551| content. Again, I always go to sea as a sailor, because they make a point of paying me for my trouble, whereas they never|122|122|
|89729|ed to sway me to my wish. With other men, perhaps, such things would not have been inducements; but as for me, I am tormen|122|122|
|90059|t the two orchard thieves entailed upon us. But being paid,—what will compare with it? The urbane activity with which a |120|122|
|90250|he springs and motives which being cunningly presented to me under various disguises, induced me to set about performing t|122|122|
|90302|rely all this is not without meaning. And still deeper the meaning of that story of Narcissus, who because he could not gr|122|122|
|92205|though I am something of a salt, do I ever go to sea as a Commodore, or a Captain, or a Cook. I abandon the glory and dist|122|122|
|92406|ight; that everybody else is one way or other served in much the same way—either in a physical or metaphysical point of |120|122|
|92638|ntially, of a broiled fowl than I will. It is out of the idolatrous dotings of the old Egyptians upon broiled ibis and roa|122|122|
|94186|iled to benches, clinched to desks. How then is this? Are the green fields gone? What do they here? But look! here come mo|122|122|
|94839| difference in the world between paying and being paid. The act of paying is perhaps the most uncomfortable infliction tha|122|122|
|95045|Coenties Slip, and from thence, by Whitehall, northward. What do you see?—Posted like silent sentinels all around the to|120|122|
|95968| who has the constant surveillance of me, and secretly dogs me, and influences me in some unaccountable way—he can bette|120|122|
|95980|h the water as they possibly can without falling in. And there they stand—miles of them—leagues. Inlanders all, they c|118|122|
|96505|the same time that the leaders little suspect it. But wherefore it was that after having repeatedly smelt the sea as a mer|122|122|
|96541|arques, brigs, schooners, and what not. And as for going as cook,—though I confess there is considerable glory in that, |120|122|
|97472|inks he breathes it first; but not so. In much the same way do the commonalty lead their leaders in many other things, at |122|122|
