<?php

define('ROOT', __DIR__);

require_once(ROOT . '/vendor/autoload.php');

ini_set('memory_limit', '1024M');

function ishmael($crypted_token, $key_data) {
    list($crypted_token, $enc_iv) = explode("::", trim($crypted_token));
    $cipher_method = 'AES-128-CTR';
    $enc_key = openssl_digest($key_data, 'SHA256', TRUE);
    $token = openssl_decrypt($crypted_token, $cipher_method, $enc_key, 0, hex2bin($enc_iv));
    unset($crypted_token, $cipher_method, $enc_key, $enc_iv);
    return $token;
}
